enum thread_state: u32(idle, think, done, exit)

enum move_result_kind(continue, is_checkmated, stalemate)

struct dispatcher_thread {
	thinkers: span<mut thinker_thread>
	
	state: thread_state
	
	; inputs (when state = .think):
	position: ^position  ; must not be modified while state = .think
	; used only while thinking:
	endgameness: f32
	best_eval: f32
	best_moves: dyn_array<move>
	; outputs (when state = .done):
	out_move_result_kind: move_result_kind
	out_best_move: move  ; if out_move_result_kind = .continue
} {
	func init_data(:^position, start_immediately: bool): self =>
		{
			.thinkers = .empty
			
			.state = if start_immediately then .think else .idle
			
			.position
			
			.endgameness = undef
			.best_eval = undef
			.best_moves = dyn_array::init.<move>()
			
			.out_move_result_kind = undef
			.out_best_move = undef
		}
	
	func create_thinkers(:^mut self, thinker_count: uptr) => (
		self.thinkers := memory:alloc_span.<thinker_thread>(thinker_count)
		for i to thinker_count do (
			self.thinkers[i] := {
				.pthread = undef
				.dispatcher = self
				.think_move = undef
				.state = .idle
				.eval = undef
				.best_moves = dyn_array::init.<move>()
			}
			discard c:pthread_create(
				^self.thinkers[i].pthread, zero, thinker_thread::c_main, cast ^self.thinkers[i])
		)
	)
	
	func start_thinking(:^mut self) => (
		self.state :=#[atomic release] .think
		futex_wake_one(^self.state.raw_value)
	)
	
	func destroy(:^mut self, pthread: c:pthread_t) => (
		while self.state#[atomic] = .think do (
			futex_wait(^self.state.raw_value, thread_state::think.raw_value)
		)
		self.state :=#[atomic] .exit
		futex_wake_one(^self.state.raw_value)
		discard c:pthread_join(pthread, zero)
	)
	
	func get_free_thinker(:^mut self): uptr => (
		for i to self.thinkers.count do (
			let thinker_state = self.thinkers[i].state#[atomic]
			if thinker_state = .done then (
				fence acquire
				self->handle_move_result(self.thinkers[i].eval, self.thinkers[i].best_moves->to_span())
				return i
			)
			if thinker_state = .idle then (
				return i
			)
		)
		~0
	)
	
	func handle_move_result(:^mut self, eval: f32, best_moves: span<move>) => (
		if eval > self.best_eval then (
			self.best_moves.count := 0
			self.best_eval := eval
		)
		if eval >= self.best_eval then (
			;best_moves->append_all(thread.best_moves)
			let dst = self.best_moves->append_array(best_moves.count)
			memory:copy.<move>(dst, best_moves)
		)
	)
	
	func handle_move(:^mut self, _: ^position, :move) => (
		loop (
			let free_thinker = self->get_free_thinker()
			if free_thinker != ~0 then (
				self.thinkers[free_thinker]->start_thinking(move)
				return
			)
			assert(c:sched_yield() = 0)  ; FIXME
		)
	)
	
	func main(:^mut self) => (
		loop (
			while self.state#[atomic] = .idle do (
				futex_wait(^self.state.raw_value, thread_state::idle.raw_value)
			)
			
			if self.state#[atomic] = .exit then (
				for i to self.thinkers.count do (
					let thinker = ^self.thinkers[i]
					;while thread.state#[relaxed] = .think {
					;	futex_wait(&thread.state.raw_value, thread_state::think.raw_value)
					;}
					thinker.state :=#[atomic] .exit
					futex_wake_one(^thinker.state.raw_value)
					;fd:eprint("Join Thinker\n")
					discard c:pthread_join(thinker.pthread, zero)
				)
				memory:free_span.<thinker_thread>(self.thinkers)
				return
			)
			
			assert(self.state#[atomic] = .think)
			fence acquire
			
			; self.out_best_move := {.src = [0, 0], .dst = [0, 0]}
			self.endgameness := get_endgameness(self.position)
			
			self.best_eval := -eval_max
			self.best_moves.count := 0
			
			enumerate_moves_for_color(self.position, AI_color,
				move_enumerator::init.<mut self>(handle_move, self))
			
			for i to self.thinkers.count do (
				let thinker = ^self.thinkers[i]
				while thinker.state#[atomic] = .think do (
					futex_wait(^thinker.state.raw_value, thread_state::think.raw_value)
				)
				if thinker.state#[atomic] != .idle then (
					assert(thinker.state#[atomic] = .done)
					fence acquire
					self->handle_move_result(thinker.eval, thinker.best_moves->to_span())
					thinker.state :=#[atomic] .idle
				)
			)
			
			if self.best_moves.count = 0 or self.best_eval <= -eval_mate - cast AI_depth * eval_mate_per_depth_left then (
				self.out_move_result_kind :=
					if position_is_safe(self.position, AI_color, .king) then .stalemate else .is_checkmated
				; fd:eprint("TODO: no moves\n")
				; c:exit(1)
			) else (
				self.out_move_result_kind := .continue
				; let w = fd::stdout->writer()
				; w->print("C7: ")
				; fmt:print_uint(w, self.position.board[1][2].raw_value)
				; w->print("\nbest moves: ")
				; for i to self.best_moves.count do (
				; 	if i > 0 then w->print(", ")
				; 	print_move(w, self.best_moves[i])
				; )
				; w->print("\n")
				
				self.out_best_move := self.best_moves[cast c:rand() % self.best_moves.count]
			)
			
			self.state :=#[atomic release] .done
			futex_wake_one(^self.state.raw_value)
			
			while self.state#[atomic] = .done do (
				futex_wait(^self.state.raw_value, thread_state::done.raw_value)
			)
		)
	)
	
	func c_main(self_: ^mut void): ^mut void => (
		main(cast self_)
		zero
	)
}

struct thinker_thread {
	pthread: c:pthread_t
	dispatcher: ^dispatcher_thread
	
	state: thread_state
	; inputs (when state = .think):
	think_move: move
	; outputs (when state = .done):
	eval: f32
	best_moves: dyn_array<move>
} {
	func start_thinking(:^mut self, :move) => (
		let state = self.state#[atomic]
		assert(state = .idle or state = .done)
		self.think_move := move
		self.state :=#[atomic release] .think
		futex_wake_one(^self.state.raw_value)
	)
	
	func main(:^mut self) => (
		loop (
			while self.state#[atomic] = .idle do (
				futex_wait(^self.state.raw_value, thread_state::idle.raw_value)
			)
			
			if self.state#[atomic] = .exit then (
				self.best_moves->uninit()
				return
			)
			
			assert(self.state#[atomic] = .think)
			fence acquire
			
			self.eval := -eval_max
			self.best_moves.count := 0
			
			let endgameness = self.dispatcher.endgameness
			let move = self.think_move
			
			var new_position = self.dispatcher.position^
			make_AI_move(^new_position, move)
			if position_is_safe(^new_position, AI_color, .king) then (
				let eval = -negamax(^new_position, endgameness, ~AI_color,
					if endgameness < 0.3 then AI_depth - 0 else
					if endgameness < 0.7 then AI_depth else
					AI_depth + 1, -1/0, 1/0)
				if eval > self.eval then (
					self.best_moves.count := 0
					self.eval := eval
				)
				if eval >= self.eval then (
					self.best_moves->append()^ := move
				)
			)
			
			self.state :=#[atomic release] .done
			futex_wake_one(^self.state.raw_value)
			
			while self.state#[atomic] = .done do (
				futex_wait(^self.state.raw_value, thread_state::done.raw_value)
			)
		)
	)
	
	func c_main(self_: ^mut void): ^mut void => (
		main(cast self_)
		zero
	)
}
