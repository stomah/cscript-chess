; ~$ ffmpeg -i chess.flac chess.wav

func assert(cond: bool) => if ~cond then unreach

;;;  SDL

; const sdl_init_timer: u32 = $01
const SDL_init_audio: u32 = $02
const SDL_init_video: u32 = $04

const SDL_window_pos_undefined: s32 = $1FFF0000

;enum sdl_mouse_button: u8(left = 1, right = 2, middle = 3, x1 = 4, x2 = 5)

const SDL_button_left: u8 = 1

;;;  END C DECLARATIONS

; struct mutex(c: c:pthread_mutex_t) {
; 	func init(self: &mut mutex) {
; 		discard c:pthread_mutex_init(&self.c, zero)
; 	}
	
; 	func uninit(self: &mut mutex) {
; 		discard c:pthread_mutex_destroy(&self.c)
; 	}
	
; 	func lock(self: &mut mutex) {
; 		discard c:pthread_mutex_lock(&self.c)
; 	}
	
; 	func unlock(self: &mut mutex) {
; 		discard c:pthread_mutex_lock(&self.c)
; 	}
; }

; struct cond_var(c: c:pthread_cond_t) {
; 	func init(self: &mut cond_var) {
; 		discard c:pthread_cond_init(&self.c, zero)
; 	}
	
; 	func uninit(self: &mut cond_var) {
; 		discard c:pthread_cond_destroy(&self.c)
; 	}
	
; 	func notify_one(self: &mut cond_var) {
; 		discard c:pthread_cond_signal(&self.c)
; 	}
	
; 	func wait(self: &mut cond_var, mutex: &mut mutex) {
; 		discard c:pthread_cond_wait(&self.c, &mutex.c)
; 	}
; }

; struct semaphore(c: c:sem_t) {
; 	func init(self: &mut semaphore) {
; 		discard c:sem_init(&self.c, 0, 0)
; 	}
	
; 	func uninit(self: &mut semaphore) {
; 		discard c:sem_destroy(&self.c)
; 	}
	
; 	func post(self: &mut semaphore) {
; 		if c:sem_post(&self.c) != 0 { unreach }
; 	}
	
; 	func wait(self: &mut semaphore) {
; 		discard c:sem_wait(&self.c)
; 	}
; }

const ai_vs_ai = false
const AI_depth: u32 = 5
const thinker_thread_count: uptr = 15
const enable_transposition_table = true
const AI_color: color = .black
const starting_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
; const starting_FEN = "k2KQ3/8/8/8/8/8/8/8 w -"
; const starting_FEN = "8/KP2r2k/8/8/8/8/8/8 w -"

enum axis_2d: bool(x, y)

enum piece_kind(pawn, knight, bishop, rook, queen, king)

enum color: bool(white, black)

struct piece equatable {raw_value: u8} {
	const none: self = {.raw_value = 255}
	
	const flag_black: u8 = 8
	
	func kind(:self): piece_kind =>
		{.raw_value = self.raw_value & ~flag_black}
	
	func color(:self): color =>
		if self.raw_value & flag_black != 0 then .black else .white
	
	func make(kind: piece_kind, :color): piece =>
		{.raw_value = kind.raw_value | if color = .black then flag_black else 0}
}

const en_passant_none: s8 = 16

enum castle_side(long, short)

func ask_promote(window: ^mut c:SDL_Window): piece_kind => (
	let buttons: [4]c:SDL_MessageBoxButtonData = [
		{
			.flags = c:SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT
			.buttonid = piece_kind::queen.raw_value
			.text = c"Queen"
		}
		{.flags = 0, .buttonid = piece_kind::rook.raw_value, .text = c"Rook"}
		{.flags = 0, .buttonid = piece_kind::bishop.raw_value, .text = c"Bishop"}
		{.flags = 0, .buttonid = piece_kind::knight.raw_value, .text = c"Knight"}
	]
	let message_box_data: c:SDL_MessageBoxData = {
		.flags = 0
		.window
		.title = c"Promotion"
		.message = c"Choose a piece to promote to"
		.numbuttons = 4
		.buttons = cast ^buttons
		.colorScheme = zero
	}
	var piece: s32 = undef
	discard c:SDL_ShowMessageBox(^message_box_data, ^piece)
	{.raw_value = cast piece}
)

type board_vec = [each axis_2d]s8

func board_vec_to_mask(v: board_vec): u64 =>
	1 << (cast v.y * 8 + cast v.x)

enum FEN_parse_status(
	success
	row_too_short
	row_too_long
	too_many_rows
	invalid_piece
	ends_too_soon
	invalid_turn
	expected_no_castle
) {
	static to_string: [each FEN_parse_status]string = {
		.success = "success"
		.row_too_short = "too few squares specified in row"
		.row_too_long = "too many squares specified in row"
		.too_many_rows = "too many rows"
		.invalid_piece = "invalid piece"
		.ends_too_soon = "expected more characters"
		.invalid_turn = "turn must be 'w' or 'b'"
		.expected_no_castle = "expected '-' or castleabilities"
	}
}

struct FEN_parse_result {status: FEN_parse_status, pos: uptr}

func parse_FEN(FEN: string, :^mut position, turn: ^mut color): FEN_parse_result => (
	var i: uptr = 0
	var pos: board_vec = [0, 0]
	position^ := {
		.board = for => for => .none
		.en_passant = en_passant_none
		.castleabilities = for => for => false
	}
	while i < FEN.bytes.count and FEN.bytes[i] != ' ' do (
		let c = FEN.bytes[i]
		if c = '/' then (
			if pos.x < 8 then return {.status = .row_too_short, .pos = i}
			if pos.y = 7 then return {.status = .too_many_rows, .pos = i}
			pos := [0, pos.y + 1]
		) else if c >= '1' and c <= '9' then (
			pos.x += cast :u8(c - '0')
			if pos.x > 8 then return {.status = .row_too_long, .pos = i}
		) else (
			let char_to_piece: [each u8]piece = {
				['p'] = piece::make(.pawn  , .black)
				['P'] = piece::make(.pawn  , .white)
				['n'] = piece::make(.knight, .black)
				['N'] = piece::make(.knight, .white)
				['b'] = piece::make(.bishop, .black)
				['B'] = piece::make(.bishop, .white)
				['r'] = piece::make(.rook  , .black)
				['R'] = piece::make(.rook  , .white)
				['q'] = piece::make(.queen , .black)
				['Q'] = piece::make(.queen , .white)
				['k'] = piece::make(.king  , .black)
				['K'] = piece::make(.king  , .white)
				.. for => .none
			}
			let piece = char_to_piece[c]
			if piece = .none then return {.status = .invalid_piece, .pos = i}
			if pos.x = 8 then return {.status = .row_too_long, .pos = i}
			position->set(pos, piece)
			pos.x += 1
		)
		i += 1
	)
	
	if pos != [8, 7] then return {.status = .ends_too_soon, .pos = i}
	
	if i = FEN.bytes.count then return {.status = .ends_too_soon, .pos = i}
	
	i += 1
	
	if i = FEN.bytes.count then return {.status = .ends_too_soon, .pos = i}
	if FEN.bytes[i] = 'w' then turn^ := .white else
	if FEN.bytes[i] = 'b' then turn^ := .black else
		return {.status = .invalid_turn, .pos = i}
	i += 1
	
	if i = FEN.bytes.count or FEN.bytes[i] != ' ' then return {.status = .ends_too_soon, .pos = i}
	i += 1
	
	if i < FEN.bytes.count and FEN.bytes[i] = 'K' then (
		position.castleabilities.white.short := true
		i += 1
	)
	if i < FEN.bytes.count and FEN.bytes[i] = 'Q' then (
		position.castleabilities.white.long := true
		i += 1
	)
	if i < FEN.bytes.count and FEN.bytes[i] = 'k' then (
		position.castleabilities.black.short := true
		i += 1
	)
	if i < FEN.bytes.count and FEN.bytes[i] = 'q' then (
		position.castleabilities.black.long := true
		i += 1
	)
	if position.castleabilities = for => for => false then (
		if ~(i < FEN.bytes.count and FEN.bytes[i] = '-') then (
			return {.status = .expected_no_castle, .pos = i}
		)
		i += 1
	)
	
	; TODO
	
	{.status = .success, .pos = i}
)

struct position {
	board: [8 indexedby s8][8 indexedby s8]piece
	en_passant: s8
	castleabilities: [each color][each castle_side]bool
} {
	func get(:^self, pos: board_vec): piece =>
		self.board[pos.y][pos.x]
	
	func set(:^mut self, pos: board_vec, :piece) =>
		self.board[pos.y][pos.x] := piece
}

func disable_castle_abilities(castleabilities: ^mut [each color][each castle_side]bool, dst: board_vec) => (
	let disable_castle_color: color =
		if dst.y = 0 then .black else
		if dst.y = 7 then .white else
			return
	let disable_castle_side: castle_side =
		if dst.x = 0 then .long else
		if dst.x = 7 then .short else
			return
	castleabilities[disable_castle_color][disable_castle_side] := false
)

;;;  Moves

struct move {src: board_vec, dst: board_vec}

func print_move(out: writer, :move) => (
	; w->print("(")
	; fmt:print_sint(w, move.src.x)
	; w->print(", ")
	; fmt:print_sint(w, move.src.y)
	; w->print(") -> (")
	; fmt:print_sint(w, move.dst.x)
	; w->print(", ")
	; fmt:print_sint(w, move.dst.y)
	; w->print(")")
	let chars = :u8[
		a"abcdefgh"[cast move.src.x]
		a"87654321"[cast move.src.y]
		a"abcdefgh"[cast move.dst.x]
		a"87654321"[cast move.dst.y]
	]
	out->write(^chars)
)

func begin_move(:^mut position, src: board_vec): piece => (
	let piece = position->get(src)
	position->set(src, .none)
	piece
)

func finish_move<promote_data>(
	:^mut position, src_piece: piece, :move,
	promote: ^func(^promote_data): piece_kind, :^promote_data,
) => (
	let kind = src_piece->kind()
	let color = src_piece->color()
	position.board[move.dst.y][move.dst.x] :=
		if kind = .pawn and (move.dst.y = 0 or move.dst.y = 7) then (
			piece::make(promote(promote_data), color)
		) else (
			src_piece
		)
	
	if kind = .king and position.castleabilities[color] != zero then (
		let rank: s8 = if color = .black then 0 else 7
		if move.dst.x = 2 then (
			position.board[rank][3] := piece::make(.rook, color)
			position.board[rank][0] := .none
		) else if move.dst.x = 6 then (
			position.board[rank][5] := piece::make(.rook, color)
			position.board[rank][7] := .none
		)
		position.castleabilities[color] := zero
	)
	if kind = .pawn and move.dst.x = position.en_passant and
		move.dst.y = if color = .white then 2 else 5 then (
		position.board[if color = .white then 3 else 4][move.dst.x] := .none
	)
	
	position.en_passant :=
		if kind = .pawn and (
			(move.src.y = 1 and move.dst.y = 3) or
			(move.src.y = 6 and move.dst.y = 4)
		) then move.dst.x else en_passant_none
	
	disable_castle_abilities(^position.castleabilities, move.dst)
)

func make_move<data>(:^mut position, :move, promote: ^func(^data): piece_kind, :^data) =>
	finish_move.<data>(position, begin_move(position, move.src), move, promote, data)

func promote_to_queen_cb(_data: ^!): piece_kind => .queen

func make_AI_move(:^mut position, :move) =>
	make_move.<!>(position, move, promote_to_queen_cb, undef)

;;;  Move Enumeration

struct move_enumerator {on_move: ^func(^mut void, ^position, move), user_data: ^mut void} {
	func init<user_data>(on_move: ^func(^user_data, ^position, move), user_data: ^user_data): self =>
		{.on_move = cast on_move, .user_data = cast user_data}
	
	func call(:self, position: ^position, move: move) =>
		self.on_move(self.user_data, position, move)
}

func enumerate_moves_in_direction(
	:^position, :color, src: board_vec, dir: board_vec, max_one: bool, cb: move_enumerator,
) => (
	var dst = src
	while (
		dst += dir
		dst.x >= 0 and dst.y >= 0 and dst.x < 8 and dst.y < 8
	) do (
		let dst_piece = position->get(dst)
		if dst_piece != .none then (
			if dst_piece->color() != color then (
				cb->call(position, {.src, .dst})
			)
			return
		)
		cb->call(position, {.src, .dst})
		if max_one then return
	)
)

func enumerate_bishop_moves(:^position, :color, src: board_vec, max_one: bool, cb: move_enumerator) => (
	let v = :board_vec[[-1, -1], [-1, 1], [1, -1], [1, 1]]
	for i to (^v).count do (
		enumerate_moves_in_direction(position, color, src, v[i], max_one, cb)
	)
)

func enumerate_rook_moves(:^position, :color, src: board_vec, max_one: bool, cb: move_enumerator) => (
	let v = :board_vec[[-1, 0], [0, 1], [0, -1], [1, 0]]
	for i to (^v).count do (
		enumerate_moves_in_direction(position, color, src, v[i], max_one, cb)
	)
)

func enumerate_moves(:^position, src: board_vec, cb: move_enumerator) => (
	let piece = position->get(src)
	let kind = piece->kind()
	let color = piece->color()
	if kind = .pawn then (
		let push: board_vec = [src.x, src.y + if color = .black then 1 else -1]
		if position->get(push) = .none then (
			cb->call(position, {.src, .dst = push})
			if src.y = (if color = .black then 1 else 6) then (
				let dst: board_vec = [push.x, if color = .black then 3 else 4]
				if position->get(dst) = .none then (
					cb->call(position, {.src, .dst})
				)
			)
		)
		
		let take_left: board_vec = [push.x - 1, push.y]
		if take_left.x >= 0 then (
			let to_take = position->get(take_left)
			if to_take != .none and to_take->color() != color then (
				cb->call(position, {.src, .dst = take_left})
			)
		)
		
		let take_right: board_vec = [push.x + 1, push.y]
		if take_right.x < 8 then (
			let to_take = position->get(take_right)
			if to_take != .none and to_take->color() != color then (
				cb->call(position, {.src, .dst = take_right})
			)
		)
		
		if push.y = (if color = .black then 5 else 2) and
				(src.x = position.en_passant - 1 or src.x = position.en_passant + 1) then (
			cb->call(position, {.src, .dst = [position.en_passant, push.y]})
		)
	) else if kind = .knight then (
		let vecs = :board_vec[[-1, -2], [-1, 2], [1, -2], [1, 2],
			[-2, -1], [2, -1], [-2, 1], [2, 1]]
		for i to (^vecs).count do (
			enumerate_moves_in_direction(position, color, src, vecs[i], true, cb)
		)
	) else if kind = .bishop then (
		enumerate_bishop_moves(position, color, src, false, cb)
	) else if kind = .rook then (
		enumerate_rook_moves(position, color, src, false, cb)
	) else if kind = .queen then (
		enumerate_bishop_moves(position, color, src, false, cb)
		enumerate_rook_moves(position, color, src, false, cb)
	) else if kind = .king then (
		enumerate_bishop_moves(position, color, src, true, cb)
		enumerate_rook_moves(position, color, src, true, cb)
		
		if position.castleabilities[color] != [false, false] then (
			let p: position = {
				.castleabilities = for => for => false
				..position^
			}
			if position_is_safe(^p, color, .king) then (
				let rank: s8 = if color = .black then 0 else 7
				
				if position.castleabilities[color].long and
					position.board[rank][0] = piece::make(.rook, color) and
					position.board[rank][1] = .none and
					position.board[rank][2] = .none and
					position.board[rank][3] = .none and
					move_is_safe(position, {.src, .dst = [3, rank]}) then (
					cb->call(position, {.src, .dst = [2, rank]})
				)
				
				if position.castleabilities[color].short and
					position.board[rank][7] = piece::make(.rook, color) and
					position.board[rank][6] = .none and
					position.board[rank][5] = .none and
					move_is_safe(position, {.src, .dst = [5, rank]}) then (
					cb->call(position, {.src, .dst = [6, rank]})
				)
			)
		)
	)
)

func enumerate_moves_for_color(:^position, :color, cb: move_enumerator) => (
	for y: s8 to 8 do (
		for x: s8 to 8 do (
			let piece = position.board[y][x]
			if piece != .none and piece->color() = color then (
				enumerate_moves(position, [x, y], cb)
			)
		)
	)
)

;;;  SAFETY

;;  unsafety is cheapest piece that cannot be taken

struct safety_data {unsafety: piece_kind, safe: bool} {
	func handle_move(:^mut self, position: ^position, move: move) => (
		let piece = position->get(move.dst)
		if piece != .none and piece->kind().raw_value >= self.unsafety.raw_value then (
			self.safe := false
		)
	)
}

func position_is_safe(:^position, :color, unsafety: piece_kind): bool => (
	var data: safety_data = {.unsafety, .safe = true}
	enumerate_moves_for_color(position, ~color,
		move_enumerator::init.<mut safety_data>(safety_data::handle_move, ^data))
	data.safe
)

func move_is_safe_for_level(:^position, :move, unsafety: piece_kind): bool => (
	var new_position = position^
	make_move.<!>(^new_position, move, promote_to_queen_cb, undef)
	position_is_safe(^new_position, position->get(move.src)->color(), unsafety)
)

func move_is_safe(:^position, :move): bool =>
	move_is_safe_for_level(position, move, .king)

;;;  TRANSPOSITION TABLE

func random_hash(seed: ^mut u64): u64 => (
	seed^ := @ *@ 6364136223846793005 +@ 1
	seed^
)

struct position_hashes {
	pieces: [each color][each piece_kind][8 indexedby s8][8 indexedby s8]u64
	castling: [each color][each castle_side]u64
	en_passant_files: [8]u64
	turns: [each color]u64
}

static position_hashes: position_hashes = (
	var seed: u64 = 6
	{
		.pieces = for => for => for => for => random_hash(^seed)
		.castling = for => for => random_hash(^seed)
		.en_passant_files = for => random_hash(^seed)
		.turns = for => random_hash(^seed)
	}
)

func hash_position(hashes: ^position_hashes, :^position, turn: color): u64 => (
	var hash: u64 = 0
	
	for y: s8 to 8 do (
		for x: s8 to 8 do (
			let piece = position->get([x, y])
			if piece != .none then (
				hash ~= hashes.pieces[piece->color()][piece->kind()][y][x]
			)
		)
	)
	
	for i: uptr to 2 do (
		let color: color = {.raw_value = cast i}
		if position.castleabilities[color].long then (
			hash ~= hashes.castling[color].long
		)
		if position.castleabilities[color].short then (
			hash ~= hashes.castling[color].short
		)
	)
	
	if position.en_passant != en_passant_none then (
		hash ~= hashes.en_passant_files[cast position.en_passant]
	)
	
	hash ~= hashes.turns[turn]
	
	if hash = 0 then 1 else hash
)

struct transposition_entry [repr: u128]  ; for atomic access
struct transposition_entry_data equatable [hash: u64, depth: u32, eval: f32]

const transposition_entry_count: uptr = 1 << 24

type transposition_table = [transposition_entry_count indexedby u64]transposition_entry

;;;  AI

func mirror_goodness(row: [8][4]u8): [8][8]u8 =>
	for y => for x => row[y][if x < 4 then x else 7 - x]

func repeat_goodness(a: [4][4]u8): [8][8]u8 =>
	for y => for x => a[if y < 4 then y else 7 - y][if x < 4 then x else 7 - x]

static opening_goodnesses: [each piece_kind][8 indexedby s8][8 indexedby s8]u8 = {
	.pawn = mirror_goodness([
		for => 100
		for => 70
		[ 5, 10, 15, 20]
		[20, 30, 45, 60]
		[25, 30, 35, 150]
		[35, 25, 40, 35]
		for => 0
		for => 0
	])
	; .knight = repeat_goodness([
	; 	[ 0,  5,  5, 10]
	; 	[10, 10, 35, 25]
	; 	[15, 25, 70, 30]
	; 	[10, 20, 30, 30]
	; ])
	.knight = mirror_goodness([
		[ 0,  0,  0,  0]
		[ 0,  0,  0,  0]
		[ 0, 10, 15, 20]
		[ 5, 20, 40, 30]
		[10, 30, 50, 50]
		[35, 25, 70, 35]
		[20, 20, 50, 40]
		[ 5, 10, 15, 10]
	])
	.bishop = mirror_goodness([
		[ 5, 10, 15, 20]
		[30, 40, 50, 40]
		[ 5, 10, 15, 20]
		[20, 50, 40, 30]
		[25, 30, 50,  5]
		[35, 25, 40, 35]
		[60, 70, 50, 40]
		[ 5, 10, 15, 20]
	])
	.rook = [
		[60, 65, 70, 75, 75, 70, 65, 60]
		for => 90
		for => 60
		for => 40
		for => 30
		for => 25
		for => 0
		[35, 10, 70, 100, 100, 80, 10, 30]
	]
	.queen = for y => for x => if y = 7 and x = 3 then 200 else 0
	; .queen = repeat_goodness([
	; 	[10, 25, 30, 40]
	; 	[25, 30, 35, 40]
	; 	[30, 20, 35, 35]
	; 	[25, 20, 20, 30]
	; ])
	.king = [
		for => 0, for => 0, for => 0, for => 0, for => 0, for => 0
		[30, 0, 0, 0, 0, 0, 0, 30]
		[50, 95, 85, 5, 30, 40, 100, 50]
	]
}

static endgame_goodnesses: [each piece_kind][8 indexedby s8][8 indexedby s8]u8 = {
	.pawn = [
		for => 240
		for => 220
		[170, 180, 190, 200, 200, 190, 180, 170]
		;[ 5, 10, 15, 20, 20, 15, 10,  5]
		[110, 130, 150, 160, 160, 150, 130, 110]
		[60, 70, 80, 100, 100, 80, 70, 60]
		[35, 25, 40, 50, 50, 40, 25, 35]
		for => 0
		for => 0
	]
	.knight = zero
	.bishop = zero
	.rook = zero
	.queen = repeat_goodness([
		[10, 25, 30, 40]
		[25, 30, 35, 40]
		[30, 35, 35, 35]
		[25, 30, 30, 30]
	])
	.king = repeat_goodness([
		[ 0, 10,  15,  20]
		[10, 20,  30,  50]
		[15, 30, 100, 120]
		[20, 50, 120, 150]
	])
}

func get_opening_goodness(:piece, dst: board_vec): u8 =>
	opening_goodnesses[piece->kind()][dst.y ~ if piece->color() = .black then 7 else 0][dst.x]

func get_endgame_goodness(:piece, dst: board_vec): u8 =>
	endgame_goodnesses[piece->kind()][dst.y ~ if piece->color() = .black then 7 else 0][dst.x]

const initial_material: u32 = (
	; piece_costs.pawn * 8 +
	piece_costs.knight * 2 +
	piece_costs.bishop * 2 +
	piece_costs.rook * 2 +
	piece_costs.queen) * 2

func get_endgameness(:^position): f32 => (   ; 0 = opening, 1 = endgame
	var material: u32 = 0
	for y: s8 to 8 do (
		for x: s8 to 8 do (
			let piece = position->get([x, y])
			if piece != .none and piece->kind() != .pawn then (
				material += piece_costs[piece->kind()]
			)
		)
	)
	if material >= initial_material then (
		0
	) else (
		1 - (cast material / cast initial_material)
	)
)

const eval_max: f32 = 1_000_000_000
const eval_mate: f32 = 100_000
const eval_mate_per_depth_left: f32 = 1_000

func map_f32(x: f32, from_min: f32, from_max: f32, to_min: f32, to_max: f32): f32 =>
	(x / (from_max - from_min) - from_min) * (to_max - to_min) + to_min

func evaluate(:^position, endgameness: f32): f32 => (
	var eval: f32 = 0
	var has_king: [each color]bool = [false, false]
	for y: s8 to 8 do (
		for x: s8 to 8 do (
			let piece = position->get([x, y])
			if piece != .none then (
				if piece->kind() = .king then (
					has_king[piece->color()] := true
				) else (
					let side_multiplier: f32 = if piece->color() = .white then 1 else -1
					let middlegame_material: f32 = 0.4
					if endgameness < middlegame_material then (
						; 0..MM -> 1..0
						eval += cast get_opening_goodness(piece, [x, y]) * map_f32(endgameness, 0, middlegame_material, 1, 0) * side_multiplier
					) else (
						; 0.MM..1 -> 0..1
						eval += cast get_endgame_goodness(piece, [x, y]) * map_f32(endgameness, middlegame_material, 1, 0, 1) * side_multiplier
					)
					eval += cast piece_costs[piece->kind()] * side_multiplier
				)
			)
		)
	)
	; if ~has_king.white then -eval_mate - eval else
	; if ~has_king.black then eval_mate - eval else
	assert(has_king.white and has_king.black)
	eval
)

func get_king_distance(:^position): f32 => (
	var king_pos: [each color]board_vec = undef
	for y: s8 to 8 do (
		for x: s8 to 8 do (
			let piece = position->get([x, y])
			if piece != .none and piece->kind() = .king then (
				king_pos[piece->color()] := [x, y]
			)
		)
	)
	let a: [each axis_2d]f32 = for i => cast :s8(king_pos.white[i] - king_pos.black[i])
	c:sqrtf(a.x*a.x + a.y*a.y)
)

;;; NEGAMAX

static piece_costs: [each piece_kind]u32 = [100, 305, 333, 563, 950, 0]

; const initial_material: u32 = (800 + 710 + 666 + 1126 + 1900) * 2

struct negamax_data {
	opponent_color: color
	endgameness: f32
	has_moves: bool
	end: bool
	next_depth: u32
	a: f32
	b: f32
} {
	func handle_move(:^mut self, :^position, :move) => (
		if self.end then return
		let dst_piece = position->get(move.dst)
		if dst_piece->kind() = .king then (
			self.a := eval_mate + cast self.next_depth * eval_mate_per_depth_left
			self.has_moves := true
			self.end := true
			return
		)
		var new_position = position^
		make_AI_move(^new_position, move)
		let depth = self.next_depth
			;if dst_piece != .none and self.next_depth = 0 { 1 } else { self.next_depth }
		let score = -negamax(^new_position, self.endgameness, self.opponent_color, depth, -self.b, -self.a)
		;if score != -eval_mate - (cast self.next_depth - 1) * 1_000_000 {
			self.has_moves := true
		;}
		if score >= self.b then (
			self.a := self.b
			self.end := true
		) else if score > self.a then (
			self.a := score
		)
	)
}

; static mut transposition_tables: [each color]transposition_table = zero
static mut transposition_table: transposition_table = zero

; struct search_captures_data(
; 	opponent_color: color
; 	endgameness: f32
; 	has_captures: bool
; 	end: bool
; 	a: f32
; 	b: f32
; ) {
; 	func handle_move(self: &mut search_captures_data, position: &position, move: move) {
; 		if self.end { return }
; 		let dst_piece = position->get(move.dst)
; 		if dst_piece = .none { return }
		
; 		self.has_captures := true
; 		if dst_piece->kind() = .king {
; 			self.a := eval_max
; 			self.end := true
; 			return
; 		}
		
; 		var new_position = position@
; 		make_AI_move(&new_position, move)
; 		let score = -search_captures(
; 			&new_position, self.endgameness, self.opponent_color, -self.b, -self.a)
		
; 		if score >= self.b {
; 			self.a := self.b
; 			self.end := true
; 		} else if score > self.a {
; 			self.a := score
; 		}
; 	}
; }

; func search_captures(position: &position, endgameness: f32, to_move: color, a: f32, b: f32) -> f32 {
; 	var data: search_captures_data = {
; 		.opponent_color = to_move->opposite()
; 		.endgameness
; 		.has_captures = false
; 		.end = false
; 		.a
; 		.b
; 	}
; 	enumerate_moves_for_color(position, to_move,
; 		move_enumerator::init.<mut search_captures_data>(search_captures_data::handle_move, &data))
; 	if ~data.has_captures {
; 		evaluate(position, endgameness) * if to_move = .white { 1 } else { -1 }
; 	} else {
; 		data.a
; 	}
; }

func negamax(:^position, endgameness: f32, to_move: color, depth: u32, a: f32, b: f32): f32 => (
	let hash = hash_position(^position_hashes, position, to_move)
	; let trans_entry = ^transposition_tables[to_move][hash % transposition_entry_count]
	let trans_entry = ^transposition_table[hash % transposition_entry_count]
	if enable_transposition_table then (
		let trans_entry_value: transposition_entry = trans_entry^#[atomic]
		let trans_entry_data: ^transposition_entry_data = cast ^trans_entry_value
		if trans_entry_data.hash = hash and trans_entry_data.depth >= depth then (
			return trans_entry_data.eval
		) else (
			; let writer = fd::stderr->writer()
			; writer->print("NOT USING TP ")
			; fmt:print_uint_hex(writer, hash, 16)
			; writer->print("|")
			; fmt:print_uint_hex(writer, trans_entry_data.hash, 16)
			; writer->print("\n")
		)
	)
	
	; if transposition_entry.hash = hash and transposition_entry.depth >= depth {
	; 	;if ~position_eq(position, &transposition_entry.position) {
	; 	;	cast puts(c"HASH COLLISION")
	; 	;}
	; 	return transposition_entry.eval
	; }
	let eval = if depth = 0 then (
		evaluate(position, endgameness) * (if to_move = .white then 1 else -1) ;+
			; get_king_distance(position) * endgameness * 10 * (if AI_color = to_move then -1 else 1)
		;search_captures(position, endgameness, to_move, -eval_max, eval_max)
	) else (
		var data: negamax_data = {
			.opponent_color = ~to_move
			.endgameness
			.has_moves = false
			.end = false
			.next_depth = depth - 1
			.a
			.b
		}
		enumerate_moves_for_color(position, to_move,
			move_enumerator::init.<mut negamax_data>(negamax_data::handle_move, ^data))
		; if ~data.has_moves then 0 else data.a
		assert(data.has_moves)  ; there should be no position with no pseudo-legal moves
		data.a
	)
	
	if enable_transposition_table then (
		var out_trans_entry: transposition_entry = undef
		let out_trans_entry_data: ^mut transposition_entry_data = cast ^out_trans_entry
		out_trans_entry_data^ := {.hash, .eval, .depth}
		trans_entry^ :=#[atomic] out_trans_entry
		
		; let trans_entry_value: transposition_entry = trans_entry^!(relaxed)
		; let trans_entry_data: ^transposition_entry_data = cast ^trans_entry_value
		; if trans_entry_data^ != out_trans_entry_data^ then (
		; 	let writer = fd::stderr->writer()
		; 	writer->print("TP error ")
		; 	fmt:print_uint_hex(writer, trans_entry_value.repr, 32)
		; 	writer->print("|")
		; 	fmt:print_uint_hex(writer, out_trans_entry.repr, 32)
		; 	writer->print(" ")
		; 	fmt:print_uint_hex(writer, hash, 16)
		; 	writer->print("|")
		; 	fmt:print_uint_hex(writer, trans_entry_data.hash, 16)
		; 	writer->print("|")
		; 	fmt:print_uint_hex(writer, out_trans_entry_data.hash, 16)
		; 	writer->print(" ")
		; 	fmt:print_uint(writer, depth)
		; 	writer->print("|")
		; 	fmt:print_uint(writer, trans_entry_data.depth)
		; 	writer->print("|")
		; 	fmt:print_uint(writer, out_trans_entry_data.depth)
		; 	writer->print("\n")
		; 	trap
		; )
	)
	
	eval
)

struct ai_negamax_data {
	endgameness: f32
	best_eval: f32
	moves: dyn_array<move>
} {
	func handle_move(:^mut self, :^position, :move) => (
		var new_position = position^
		make_AI_move(^new_position, move)
		if ~position_is_safe(^new_position, AI_color, .king) then return
		
		let eval = -negamax(^new_position, self.endgameness, ~AI_color,
			if self.endgameness < 0.3 then AI_depth - 0 else
			if self.endgameness < 0.7 then AI_depth else
			AI_depth + 1, -1/0, 1/0)
		if eval > self.best_eval then (
			self.moves.count := 0
			self.best_eval := eval
		)
		if eval >= self.best_eval then (
			self.moves->append()^ := move
		)
	)
}

func ai(:^position, :color, :^mut move): bool => (
	var data: ai_negamax_data = {
		.endgameness = get_endgameness(position)
		.best_eval = -eval_max
		.moves = dyn_array::init.<move>()
	}
	
	enumerate_moves_for_color(position, color,
		move_enumerator::init.<mut ai_negamax_data>(ai_negamax_data::handle_move, ^data))
	
	let writer = fd::stdout->writer()
	fmt:print_sint(writer, cast (data.endgameness * 1000))
	writer->print(", eval = ")
	fmt:print_sint(writer, cast data.best_eval)
	writer->print("\n")
	
	if data.moves.count > 0 then (
		move^ := data.moves[cast c:rand() % data.moves.count]
	)
	data.moves->uninit()
	data.moves.count > 0
)

static piece_xs: [each piece_kind]u16 = [1000, 600, 400, 800, 200, 0]

func render(
	renderer: ^mut c:SDL_Renderer
	pieces_texture: ^mut c:SDL_Texture
	position: ^position
	moves: u64
	AI_move_piece: piece
	AI_move_begin: u64
	AI_move_end: u64
	AI_move: ^move
) => (
	discard c:SDL_SetRenderDrawColor(renderer, 215, 215, 215, 255)
	discard c:SDL_RenderClear(renderer)
	
	discard c:SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255)
	for half_y: s32 to 4 do (
		for x: s32 to 8 do (
			let rect: c:SDL_Rect = {.x, .y = half_y * 2 + (x & 1), .w = 1, .h = 1}
			discard c:SDL_RenderFillRect(renderer, ^rect)
		)
	)
	
	discard c:SDL_SetRenderDrawColor(renderer, 50, 200, 100, 100)
	for y: s8 to 8 do (
		for x: s8 to 8 do (
			let piece = position.board[y][x]
			let dst: c:SDL_Rect = {.x, .y, .w = 1, .h = 1}
			if moves & board_vec_to_mask([x, y]) != 0 then (
				discard c:SDL_RenderFillRect(renderer, ^dst)
			)
			if piece != .none then (
				let src: c:SDL_Rect = {
					.x = piece_xs[piece->kind()]
					.y = if piece->color() = .black then 200 else 0
					.w = 200
					.h = 200
				}
				discard c:SDL_RenderCopy(renderer, pieces_texture, ^src, ^dst)
			)
		)
	)
	
	if AI_move_piece != .none then (
		let ticks = c:SDL_GetTicks64()
		let time_from_begin: u64 = ticks - AI_move_begin
		let time_to_end: u64 = AI_move_end - ticks
		let progress: f32 = cast time_from_begin / cast :u64(AI_move_end - AI_move_begin)
		let p: [each axis_2d]f32 =
			for i => (1 - progress) * cast AI_move.src[i] + progress * cast AI_move.dst[i]
		
		let time_to_closest: f32 =
			cast (if time_from_begin < time_to_end then time_from_begin else time_to_end) / 500
		let size = time_to_closest * 2 + 1
		let dst: c:SDL_FRect = {
			.x = p.x - time_to_closest
			.y = p.y - time_to_closest
			.w = size
			.h = size
		}
		let src: c:SDL_Rect = {
			.x = piece_xs[AI_move_piece->kind()]
			.y = if AI_move_piece->color() = .black then 200 else 0
			.w = 200
			.h = 200
		}
		discard c:SDL_RenderCopyF(renderer, pieces_texture, ^src, ^dst)
	)
	
	c:SDL_RenderPresent(renderer)
)

func set_safe_moves_cb(safe_moves: ^mut u64, :^position, :move) => (
	if move_is_safe(position, move) then (
		safe_moves^ |= board_vec_to_mask(move.dst)
	)
)

func benchmark_on_move(_data: ^!, _: ^position, :move) => (
	var m: move = undef
	m :=#[volatile] move
)

func benchmark_move_generation(): u64 => (
	let begin_time = c:SDL_GetTicks64()
	let position: position = {
		.en_passant = en_passant_none
		.castleabilities = for => for => false
		.board = [
			[.none, .none, piece::make(.rook, .black), piece::make(.queen, .black), .none, piece::make(.rook, .black), piece::make(.king, .black), .none]
			[piece::make(.rook, .white), .none, .none, piece::make(.knight, .black), .none, piece::make(.pawn, .black), piece::make(.pawn, .black), piece::make(.pawn, .black)]
			[.none, .none, .none, .none, piece::make(.pawn, .black), .none, .none, .none]
			[.none, .none, piece::make(.pawn, .black), piece::make(.bishop, .black), .none, .none, .none, .none]
			[.none, .none, .none, .none, .none, piece::make(.bishop, .white), .none, .none]
			[.none, .none, .none, .none, .none, .none, piece::make(.pawn, .white), .none]
			[.none, piece::make(.queen, .white), .none, .none, piece::make(.pawn, .white), piece::make(.pawn, .white), piece::make(.bishop, .white), piece::make(.pawn, .white)]
			[.none, .none, .none, piece::make(.rook, .white), .none, .none, piece::make(.king, .white), .none]
		]
	}
	for _: uptr to 10_000_000 do (
		enumerate_moves_for_color(^position, .white,
			move_enumerator::init.<!>(benchmark_on_move, undef))
	)
	c:SDL_GetTicks64() - begin_time
)

pub func main(): s32 => (
	var :position = undef
	var turn: color = undef
	let FEN_parse_result = parse_FEN(starting_FEN, ^position, ^turn)
	if FEN_parse_result.status != .success then (
		let out = fd::stderr->writer()
		out->print("FAILED TO PARSE FEN: ")
		fmt:print_uint(out, FEN_parse_result.pos)
		out->print(": ")
		out->print(FEN_parse_status::to_string[FEN_parse_result.status])
		out->print("\n")
		return 1
	)
	
	c:srand(cast c:time(zero))
	discard c:SDL_Init(SDL_init_video | SDL_init_audio)
	discard c:IMG_Init(cast c:IMG_INIT_PNG)
	
	if false then (
		let w = fd::stdout->writer()
		w->print("running move generation benchmark...\n")
		
		let bench_time = benchmark_move_generation()
		
		w->print("bench = ")
		fmt:print_uint(w, bench_time)
		w->print("ms\n")
		return 3
	)
	
	let window = c:SDL_CreateWindow(c"CScript Chess",
		SDL_window_pos_undefined, SDL_window_pos_undefined, 800, 800,
		c:SDL_WINDOW_ALLOW_HIGHDPI)
	let renderer = c:SDL_CreateRenderer(window, -1, c:SDL_RENDERER_PRESENTVSYNC)
	discard c:SDL_SetRenderDrawBlendMode(renderer, c:SDL_BLENDMODE_BLEND)
	let pieces_surface = c:IMG_Load(c"../chess_pieces.png")
	if pieces_surface = zero then (
		fd:eprint("failed to load pieces image\n")
		return 1
	)
	let pieces_texture = c:SDL_CreateTextureFromSurface(renderer, pieces_surface)
	discard c:SDL_SetTextureScaleMode(pieces_texture, c:SDL_ScaleModeLinear)
	discard c:SDL_RenderSetLogicalSize(renderer, 8, 8)
	
	var audio_spec: c:SDL_AudioSpec = undef
	var audio_buffer: ^mut u8 = undef
	var audio_size: u32 = undef
	let audio_rw = c:SDL_RWFromFile(c"../chess.wav", c"rb")
	let has_sound = c:SDL_LoadWAV_RW(audio_rw, 1, ^audio_spec, ^audio_buffer, ^audio_size) != zero
	let audio_device = if has_sound then (
		let device = c:SDL_OpenAudioDevice(zero, 0, ^audio_spec, ^audio_spec, 0)
		c:SDL_PauseAudioDevice(device, 0)
		device
	) else (
		fd:eprint("failed to load sound file\n")
		undef
	)
	
	var begin_drag: board_vec = undef
	var possible_moves: u64 = 0
	
	var dispatcher_thread = dispatcher_thread::init_data(^position, AI_color = turn)
	dispatcher_thread->create_thinkers(thinker_thread_count)
	var dispatcher_pthread: c:pthread_t = undef
	discard c:pthread_create(
		^dispatcher_pthread, zero, dispatcher_thread::c_main, cast ^dispatcher_thread)
	
	; var ai_data: ai_thread_data = {
	; 	.mutex = undef
	; 	.cond = undef
	; 	.position = starting_position
	; 	.turn = starting_turn;.white
	; 	.pending = AI_color = starting_turn
	; 	.in_game = true
	; 	.AI_move = undef
	; }
	; discard c:pthread_mutex_init(&ai_data.mutex, zero)
	; discard c:pthread_cond_init(&ai_data.cond, zero)
	; discard c:pthread_cond_signal(&ai_data.cond)
	; var ai_thread: c:pthread_t = undef
	; discard c:pthread_create(&ai_thread, zero, ai_thread_func, cast &ai_data)
	
	var old_position = position
	
	var AI_move_begin: u64 = 0
	var AI_move_end: u64 = 0
	var AI_move_piece: piece = .none
	var AI_move: move = undef
	
	var held_piece: piece = undef
	var AI_think_begin_time: u64 = undef
	
	;var last_ticks = c:SDL_GetTicks64()
	
	var run = true
	while run do (
		; discard c:pthread_mutex_lock(&ai_data.mutex)
		; if ai_data.turn = AI_color and ~ai_data.pending and ai_data.in_game {
		; 	AI_move := ai_data.AI_move
		; 	ai_data.turn := AI_color->opposite()
		; 	AI_move_begin := c:SDL_GetTicks64()
		; 	let d: board_vec = ai_data.AI_move.src - ai_data.AI_move.dst
		; 	let s = c:sqrtf(c:fabsf(cast d.x) + c:fabsf(cast d.y))
		; 	AI_move_end := AI_move_begin + 150 + cast (s * 100)
		; 	AI_move_piece := begin_move(position, ai_data.AI_move.src)
		; }
		; discard c:pthread_mutex_unlock(&ai_data.mutex)
		if dispatcher_thread.state#[atomic] = .done then (
			fence acquire
			turn := ~AI_color
			(
				let out = fd::stdout->writer()
				out->print("AI eval: ")
				fmt:print_sint(out, cast (dispatcher_thread.best_eval * 1000))
				out->print("\n")
			)
			if dispatcher_thread.out_move_result_kind = .stalemate then (
				fd:print("GAME ENDS IN STALEMATE\n")
			) else
			if dispatcher_thread.out_move_result_kind = .is_checkmated then (
				fd:print("GAME ENDS IN CHECKMATE (you win!)\n")
			) else
			if dispatcher_thread.out_move_result_kind = .continue then (
				AI_move := dispatcher_thread.out_best_move
				;futex_wake_one(&dispatcher_thread.state.raw_value)
				
				AI_move_begin := c:SDL_GetTicks64()
				let d: board_vec = AI_move.src - AI_move.dst
				let s = c:sqrtf(c:fabsf(cast d.x) + c:fabsf(cast d.y))
				AI_move_end := AI_move_begin + 150 + cast (s * 100)
				AI_move_piece := begin_move(^position, AI_move.src)
				
				let out = fd::stdout->writer()
				out->print("AI move: ")
				print_move(out, AI_move)
				out->print(" in ")
				fmt:print_uint(out, c:SDL_GetTicks64() - AI_think_begin_time)
				out->print("ms\n")
			) else
			unreach
			dispatcher_thread.state :=#[atomic] .idle
		)
		
		var event: c:SDL_Event = undef
		while c:SDL_PollEvent(^event) != 0 do (
			let common_event: ^c:SDL_CommonEvent = cast ^event
			let event_type: c:SDL_EventType = common_event.type
			if event_type = c:SDL_QUIT then (
				run := false
				; discard c:pthread_mutex_lock(&ai_data.mutex)
				; ai_data.in_game := false
				; discard c:pthread_mutex_unlock(&ai_data.mutex)
				; discard c:pthread_cond_signal(&ai_data.cond)
			) else if ~ai_vs_ai and turn = ~AI_color and ;ai_data.in_game and
				(event_type = c:SDL_MOUSEBUTTONDOWN or event_type = c:SDL_MOUSEBUTTONUP) then (
				let button_event: ^c:SDL_MouseButtonEvent = cast ^event
				if button_event.button = SDL_button_left then (
					if AI_move_piece != .none then (
						finish_move.<!>(^position, AI_move_piece, AI_move, promote_to_queen_cb, undef)
						AI_move_piece := .none
					)
					var mouse_x: s32 = undef
					var mouse_y: s32 = undef
					discard c:SDL_GetMouseState(^mouse_x, ^mouse_y)
					let p: board_vec = [cast :s32(mouse_x / 100), cast :s32(mouse_y / 100)]
					if button_event.state != 0 then (
						begin_drag := p
						possible_moves := 0
						held_piece := position->get(p)
						if held_piece != .none and held_piece->color() = ~AI_color then (
							let pieces_data: ^[]u32 = cast pieces_surface.pixels
							let f = pieces_surface.format
							let y: uptr = if AI_color = .white then 200 else 0
							let surface = c:SDL_CreateRGBSurfaceFrom(
								cast ^pieces_data[piece_xs[held_piece->kind()] +
									cast pieces_surface.pitch / 4 * y],
								200, 200, f.BitsPerPixel, pieces_surface.pitch,
								f.Rmask, f.Gmask, f.Bmask, f.Amask)
							c:SDL_SetCursor(c:SDL_CreateColorCursor(surface, 100, 100))
							c:SDL_FreeSurface(surface)
							enumerate_moves(^position, p,
								move_enumerator::init.<mut u64>(set_safe_moves_cb, ^possible_moves))
							old_position := position
							discard begin_move(^position, begin_drag)
						)
					) else (
						let old_cursor = c:SDL_GetCursor()
						c:SDL_SetCursor(c:SDL_GetDefaultCursor())
						c:SDL_FreeCursor(old_cursor)
						if possible_moves & board_vec_to_mask(p) != 0 then (
							if has_sound then (
								discard c:SDL_QueueAudio(audio_device, cast audio_buffer, audio_size)
							)
							finish_move.<mut c:SDL_Window>(^position, held_piece,
								{.src = begin_drag, .dst = p}, ask_promote, window)
							
							if true then (
								dispatcher_thread->start_thinking()
								AI_think_begin_time := c:SDL_GetTicks64()
								turn := AI_color
							) else (
								if ai(^position, AI_color, ^AI_move) then (
									AI_move_begin := c:SDL_GetTicks64()
									let d: board_vec = AI_move.src - AI_move.dst
									let s = c:sqrtf(c:fabsf(cast d.x) + c:fabsf(cast d.y))
									AI_move_end := AI_move_begin + 150 + cast (s * 100)
									AI_move_piece := begin_move(^position, AI_move.src)
								)
							)
							; discard c:pthread_mutex_lock(&ai_data.mutex)
							; ai_data.pending := true
							; discard c:pthread_mutex_unlock(&ai_data.mutex)
							; discard c:pthread_cond_signal(&ai_data.cond)
						) else (
							position->set(begin_drag, held_piece)
						)
						held_piece := .none
						possible_moves := 0
					)
				)
			) else if event_type = c:SDL_KEYDOWN then (
				let keyboard_event: ^c:SDL_KeyboardEvent = cast ^event
				if keyboard_event.keysym.scancode = c:SDL_SCANCODE_SPACE then (
					if turn = ~AI_color and held_piece = .none then (
						if AI_move_piece != .none then (
							finish_move.<!>(^position, AI_move_piece, AI_move, promote_to_queen_cb, undef)
							AI_move_piece := .none
						)
						position := old_position
					)
				; } else if keyboard_event.keysym.scancode = SDL_SCANCODE_UP {
				; 	if ai_turn_time > 50 {
				; 		ai_turn_time := ai_turn_time - 50
				; 	}
				; } else if keyboard_event.keysym.scancode = SDL_SCANCODE_DOWN {
				; 	if ai_turn_time < 2000 {
				; 		ai_turn_time := ai_turn_time + 50
				; 	}
				)
			)
		)
		
		if ~ai_vs_ai and AI_move_piece != .none then (
			assert(turn != AI_color)
			let ticks = c:SDL_GetTicks64()
			if ticks >= AI_move_end then (
				finish_move.<!>(^position, AI_move_piece, AI_move, promote_to_queen_cb, undef)
				AI_move_piece := .none
			)
		)
		;if ai_vs_ai and in_game {
		;	let new_ticks = SDL_GetTicks64()
		;	if new_ticks - ai_timer >= ai_turn_time {
		;		ai_timer := new_ticks
		;		var AI_move move = undef
		;		in_game := ai(&position, ai_turn, move_number, true, ai_turn = .black, &AI_move)
		;		if in_game {
		;			make_move.<u8>(&position, AI_move.src, AI_move.dst, |_| .queen, undef)
		;		}
		;		if ai_turn = .black {
		;			move_number := move_number + 1
		;		}
		;		ai_turn := ~ai_turn
		;	}
		;}
		render(renderer, pieces_texture, ^position, possible_moves,
			AI_move_piece, AI_move_begin, AI_move_end, ^AI_move)
		
		; let new_ticks = c:SDL_GetTicks64()
		; let writer = fd::stdout->writer()
		; writer->print("Delta time = ")
		; fmt:print_uint(writer, new_ticks - last_ticks)
		; writer->print("ms\n")
		; last_ticks := new_ticks
	)
	
	dispatcher_thread->destroy(dispatcher_pthread)
	;cast pthread_cond_destroy(&ai_data.cond)
	;cast pthread_mutex_destroy(&ai_data.mutex)
	;cast pthread_join(ai_thread, zero)
	
	if has_sound then (
		c:SDL_CloseAudioDevice(audio_device)
		c:SDL_FreeWAV(audio_buffer)
	)
	
	c:SDL_DestroyTexture(pieces_texture)
	c:SDL_FreeSurface(pieces_surface)
	c:SDL_DestroyRenderer(renderer)
	c:SDL_DestroyWindow(window)
	c:IMG_Quit()
	c:SDL_Quit()
	0
)
