func(no_link_prefix) syscall(n: sptr): sptr extern

const SYS_futex: sptr = 202

const futex_op_wait: s32 = 0
const futex_op_wake: s32 = 1
const futex_op_flag_private: s32 = 128

func futex_wait(p: ^u32, undesired_value: u32) =>
	discard :^func(sptr, ^u32, s32, u32, ^void): sptr(cast syscall)(SYS_futex,
		p, futex_op_wait | futex_op_flag_private, undesired_value, zero)

func futex_wake_one(p: ^u32) =>
	discard :^func(sptr, ^u32, s32, u32): sptr(cast syscall)(SYS_futex,
		p, futex_op_wake | futex_op_flag_private, 1)
