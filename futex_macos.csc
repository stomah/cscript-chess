func(no_link_prefix) __ulock_wait(op: u32, addr: ^u32, value: u64, timeout: u32): s32 extern
func(no_link_prefix) __ulock_wake(op: u32, addr: ^u32, value: u64): s32 extern

func futex_wait(p: ^u32, undesired_value: u32) =>
	assert(__ulock_wait(1, p, undesired_value, 0) >= 0)

func futex_wake_one(p: ^u32) => (
	let result = __ulock_wake(1 | $01000000, p, 0)
	if result < 0 and result != -2 then (
		let writer = fd::stderr->writer()
		writer->print("Futex Wake error (")
		fmt:print_sint(writer, result)
		writer->print(")\n")
		trap
	)
)
