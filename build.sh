set -e
CSCMI=/usr/local/cscmi

DEPS="cscript-std sdl2 SDL2_image"
CFLAGS=`pkg-config --libs $DEPS`
CSCFLAGS="`pkg-config --cflags-only-I $DEPS` --sloppy --target-cpu-features +cx16"

if [[ $OSTYPE == 'darwin'* ]]; then
	FUTEX_CSC="futex_macos.csc"
else
	FUTEX_CSC="futex_linux.csc"
fi

cscc chess.csc threads.csc $FUTEX_CSC $CSCFLAGS -b chess.o -c include.h -l =$CSCMI/core.cscmi -l $CSCMI/string.cscmi -l =$CSCMI/core_io.cscmi -l $CSCMI/memory.cscmi -l fmt=$CSCMI/format.cscmi -l $CSCMI/fd.cscmi -l $CSCMI/dyn_array.cscmi
clang -flto -O2 -fsanitize=undefined -fuse-ld=lld chess.o $CFLAGS -lSDL2 -lSDL2_image -lm -o chess
